figures_multitest_peter_all_panels_one_file <- function(mainDir, dataDir, figuresDir) {
  # This script creates figures for the MULTITEST paper first authored by Peter Domonkos.
  # There are three such scripts, this one creates one large file for all panels, which was useful for quick checks during development.
  # 
  # The data files have the columns:
  # Test [which is a running number] Raw	AC3	AC4	Cl1	Cl2	MSm	MSy	PHA	RHT	RHQ
  # From the distributions of all tests we compute percentiles (and an average) for each of the data columns.
  # There are data files for different quality assessment metrics and for different selections of tests.
  # The names of the files correspond to the panels in the paper.
  
  # Determine input file names and output figure names
  if( missing("mainDir"))    mainDir    = '/home/victor/Documents/projects/2020_multitest_boxplots/'
  if( missing("dataDir"))    dataDir    = '/home/victor/Documents/projects/2020_multitest_boxplots/M12-Figs-csv/'
  if( missing("figuresDir")) figuresDir = '/home/victor/Documents/projects/2020_multitest_boxplots/figures/'
  if (dir.exists(figuresDir) == F ) dir.create(figuresDir)
  source(paste0(mainDir, "percentile_boxplot.R"))
  
  fich = dir(dataDir, '*\\.csv') # Retrieve file names of the data files
  fig  = sapply(strsplit(fich,'[-\\.]'), function(x) x[2]) # Figure names
  
  # Initialize
  upperLims    = c(2, 2, 6, 2.4, 0.8, 2)                    # Upper Y axis limit (ylim)
  panelLabels = letters[1:5]                                # Panel labels
  xLabels = c('RMSEm [°C]', 'RMSEy [°C]', 'Trb [°C / 100 yrs]', 'NetTr [°C / 100 yrs]', 'NetEy [°C]', 'SysTr [°C]') # X-axis labels RMSEm, b) RMSEy, c) Trb, d) NetTr, e) NetEy.
  pdf(paste0(figuresDir, 'figures_multitest12_all_panels.pdf')) # All figures in a single document # width=7, height=5
  nFig = length(fig)
  
  # Main loop over all figures and panels
  for(k in 1:nFig) {
    # Read file
    data = read.csv(paste0(dataDir, fich[k])); 
    nc   = ncol(data)
    data = data[, 2:nc]; # Remove the first column with the running test number.
    nc  = nc-1
    
    # Determine figure labels [z] and panel number [j]
    z = substring(fig[k], 5) # z is mostly the last character of the figure name, for figure 7 it does not exist and in the next line is set to
    if(z=='') j = 6 else j = which(panelLabels==z)
    
    if(z=="") { # Figure 7 has less contributions and should be less high.
      margins = c(15,6,4,1)+0.1
      ylims = c(1-nc*0.1, nc*1.1)        # The y limits have to be made a big bigger, the outer boxplots are otherwise too close to the edge when using the plot command. boxplot() does this automatically.
    } else {
      margins = c(5,6,4,1)+0.1           # margin: bottom, left, top, right
      ylims = c(1-nc*0.01, nc*1.01)      # The y limits have to be made a big bigger, the outer boxplots are otherwise too close to the edge when using the plot command. boxplot() does this automatically.
    }
    
    if(k < nFig) {
      highlightRaw = T
    } else {
      highlightRaw = F
    }
    percentile_boxplot(data, panelLabels, xLabels[j], upperLims[j], margins, ylims, highlightRaw)
    
    # Plot figure name and panel labels
    title(fig[k])
    if(z!="") { # Figure 7 has only one panel, does not need a sub panel label.
      labelstr = paste0("(", z, ")")
      text(upperLims[j]*0.98, nc*0.9, labelstr, adj=1, cex=1.5)
    } 
  } # End main loop over all k figures
  
  dev.off() # All figures in a single document
} # End of function

